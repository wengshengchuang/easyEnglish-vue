import TeaHome from '../page/TeaHome.vue'
import AddTest from '../page/AddTest.vue'
import Success from '../page/Success.vue'
import CourseManage from '../page/CourseManage.vue'
import CourseInfo from '../page/CourseInfo.vue'
import CoursePic from '../page/CoursePic.vue'
import Teachplan from '../page/Teachplan.vue'
import MyCourse from '../page/MyCourse.vue'

export default [{
    path: '/tea', component: TeaHome,
    beforeEnter: (to, from, next) => {
      // ...
      // if(window.sessionStorage.getItem("token") == undefined)

      next()
    },
    children: [
      {
        path: '/test/add', component: AddTest
      },{
        path: '/test/add/success', component: Success
      },{
        path: '/tea/courseManage', component: CourseManage,
        
        children:[
          {
            path: '/tea/courseManage/new/courseInfo', component:CourseInfo
          },{
            path: '/tea/courseManage/:courseId/courseInfo', component:CourseInfo
          },{
            path: '/tea/courseManage/:courseId/coursePic', component:CoursePic
          },{
            path: '/tea/courseManage/:courseId/teachplan', component:Teachplan
          }
        ]
      },{
        path: '/tea/courseManage/:courseId', component: CourseManage
      },{
        path: 'myCourse', component: MyCourse
      }
    ]
  }
]