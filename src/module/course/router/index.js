import Index from '@/base/page/Index.vue'
import Course from '../page/Course.vue'
import CourseList from '../page/CourseList.vue'
import Profile from '../page/Profile.vue'

export default [
    {
        path: '/',
        component: Index,
        children: [
            {
                path: '/course',
                component: CourseList,
            },{
                path: '/course/:courseId', component: Profile
            }, {
                path: '/course/:courseId/:teachplanId', component: Course
            }
        ]
    }

]