import AdminHome from '../page/AdminHome.vue'
import CheckCourse from '../page/CheckCourse.vue'

export default [{
    path:'/admin', component: AdminHome,
    beforeEnter: (to, from, next) => {
        // ...
        // if(window.sessionStorage.getItem("token") == undefined)
  
        next()
      },
      children:[
          {
              path:'/admin/checkCourse', component: CheckCourse
          }
      ]
}]