import Index from '@/base/page/Index.vue'
import StuCourse from '../page/StuCourse.vue'

export default [
    {
        path:'/',
        component: Index,
        children: [
            {
                path: '/stu/course/:stuId',
                component: StuCourse
            }
        ]
    }
]