import Test from '../page/Test.vue'
import Summary from '../page/Summary.vue'
import Question from '../page/Question.vue'
import AnsSheet from '../page/AnsSheet.vue'
import Analytic from '../page/Analytic.vue'
import Answer from '../page/Answer.vue'
import Index from '@/base/page/Index.vue'

export default [
    {
        path: '/',
        component: Index,
        children: [
            {
                path: '/test',
                component: Test
            }, {
                path: '/test/summary/:testId',
                name: 'summary',
                component: Summary
            }, {
                path: '/test/question/:testId',
                name: 'question',
                component: Question,
                // beforeEnter: (to, from, next) => {

                // }
            }, {
                path: '/test/anssheet/:ansSheetId',
                name: 'anssheet',
                component: AnsSheet,
                children: [
                    {
                        path: '/test/:testId/anssheet/:ansSheetId/analytic',
                        component: Analytic
                    }, {
                        path: '/test/:testId/anssheet/:ansSheetId/answer/:index',
                        component: Answer
                    }
                ]
            }
        ]
    }]