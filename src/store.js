import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: sessionStorage.getItem('state') ? JSON.parse(sessionStorage.getItem('state')) : {
        // userstatus: false,
        loginDialogVisible: false,
        registDialogVisible: false,
        backpath: undefined,

        currentUser: null,
        isLogin: false,
        token: "",

    },
    mutations: {
        // reveUserstatus(state) {
        //     state.userstatus = !state.userstatus;
        // },
        openLoginDialog(state) {
            state.loginDialogVisible = true;
        },
        closeLoginDialog(state) {
            state.loginDialogVisible = false;
        },
        openRegistDialog(state) {
            state.registDialogVisible = true;
        },
        closeRegistDialog(state) {
            state.registDialogVisible = false;
        },
        userstatus(state, user, token) {
            if (user) {
                state.currentUser = user;
                state.token = token;
                state.isLogin = true;
            } else if (user == null) {
                sessionStorage.removeItem('username');
                sessionStorage.removeItem('token');
                state.currentUser = null;
                state.isLogin = false;
                state.token = "";
            }
        },

        setBackpath(state, path){
            state.backpath=path;
        }

    },
    actions: {
        setUser(context, user, token) {
            context.commit('userstatus', user, token);
        }
    },
    getters: {
        currentUser(state) {
            return state.currentUser
        },
        isLogin(state) {
            return state.isLogin
        },

    },
})