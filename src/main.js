import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './base/router/index.js'
import './plugins/element.js'
import axios from 'axios'
import { Message } from 'element-ui'
import store from './store'
import VePie from 'v-charts/lib/pie.common'
import { Affix, Result,Button } from 'ant-design-vue'
import NodataCard from '@/base/components/NodataCard.vue'

const router = new VueRouter({
  routes:routes
})
Vue.prototype.$http = axios
Vue.prototype.$message= Message
Vue.component(VePie.name, VePie)
Vue.component(Affix.name, Affix)
Vue.component(Result.name, Result)
Vue.component(Button.name, Button)
Vue.component("nodata-card", NodataCard)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
