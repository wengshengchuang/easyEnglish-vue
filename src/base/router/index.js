import Vue from 'vue';
import Router from 'vue-router';
import C404 from '../page/404.vue'
import Index from '../page/Index.vue'

Vue.use(Router);
// 定义路由配置
let routes = [{
    path: '/',
    component: Index,
    hidden: true,
    children: [{
        path: '/404', component: C404
    }

    ]
}]
let concat = (router) => {
    routes = routes.concat(router)
}

import CourseRouter from '../../module/course/router/index.js'
import TestRouter from '../../module/test/router/index.js'
import TeaRouter from '../../module/tea/router/index.js'
import AdminRouter from '../../module/admin/router/index.js'
import StuRouter from '../../module/stu/router/index.js'
concat(CourseRouter)
concat(TestRouter)
concat(TeaRouter)
concat(AdminRouter)
concat(StuRouter)
export default routes;